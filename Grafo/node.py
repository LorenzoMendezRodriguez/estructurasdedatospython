from connexion import *

class Node:

	def __init__(self,name_node):
		self.listOfConnexions = []
		self.name = name_node




	def getName(self):
		return self.name



	def setName(self,name_node):
		self.name = name_node



	def getConnexion(self):
		return self.listOfConnexions



	def setConnexion(self,destination,ponderation=0):
		connexion = Connexion(destination,ponderation)
		self.listOfConnexions.append(connexion)




	def deleteConnexionInPosition(self,connexion_pos):
		self.listOfConnexions.remove(self.listOfConnexions[connexion_pos])




	def deleteConnexionWhithName(self,destination_name):
		for connexion in self.listOfConnexions:
			if connexion.getDestination().getName() == destination_name:
				self.listOfConnexions.remove(connexion)
			else:
				print("Edge not found")




	def __str__(self):
		chain = self.name +": \n"
		for connexion in self.listOfConnexions:
			chain += str(connexion)+"\n"
		return chain