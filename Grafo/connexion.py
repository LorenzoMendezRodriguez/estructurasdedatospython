class Connexion:

	def __init__(self,destination_node,ponderation_node):
		self.__destination = destination_node
		self.__ponderation = ponderation_node



	def getDestination(self):
		return self.__destination




	def setDestination(self,new_destination):
		self.__destination = new_destination




	def getPonderation(self):
		return self.__ponderation




	def setPonderation(self,new_ponderation):
		self.__ponderation = new_ponderation




	def __str__(self):
		chain = self.__destination.getName() +", "+ str(self.__ponderation)
		return chain
