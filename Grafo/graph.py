#References: http://interactivepython.org/runestone/static/pythoned/Graphs/Implementacion.html
#https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_Java/Ap%C3%A9ndices/Implementaci%C3%B3n_del_Algoritmo_de_Kruskal_en_Java
from edge import *
from node import * 
class Graph:

	#Input boolean value :
	#For a graph
	#GUIDED_GRAPH = True
	#UNGUIDED_GRAPH = False
	def __init__(self,graph_type):
		self.nodesList = {} #HashTable
		self.adjacencyList = []
		self.numNodes = 0
		self.numEdges = 0
		self.graph_type = graph_type



	def setNode(self,node_name):
		self.numNodes+=1
		newNode = Node(node_name)
		self.nodesList[node_name] = newNode



	def getNode(self,node_name):
		return self.nodesList[node_name]



	def deleteNode(self,node_name):
		del self.nodesList[node_name]



	def getEdges(self):
		return self.adjacencyList


  
	def getNodes(self):
		return self.nodesList




	def setEdge(self,origin,destination,weight):
		if self.graph_type:

			edge = Edge(origin, destination, weight)

			self.nodesList[origin].setConnexion(self.nodesList[destination], weight)

			self.adjacencyList.append(edge)

			self.numEdges+=1

		else:

			origin_node_edge = Edge(origin, destination, weight)
			destination_node_edge = Edge(destination, origin, weight)

			self.nodesList[origin].setConnexion(self.nodesList[destination], weight)
			self.nodesList[destination].setConnexion(self.nodesList[origin], weight)

			self.adjacencyList.append(origin_node_edge)
			self.adjacencyList.append(destination_node_edge)

			self.numEdges+=1




	def __str__(self):
		chain = ""
		for node in self.nodesList.values():
			chain += str(node)+"\n"
		return chain



