from node import *
#http://www.pythondiario.com/2018/07/arbol-binario-de-busqueda-estructura-de.html
class BinaryTree:

	def __init__(self):
		self.root = None
		self.size = 0


	def insert(self,key,value):
		new_node = Node(key, value)

		if self.root == None:
			self.root = new_node

		else:
			self.__insert_aux(new_node,self.root)
		self.size+=1


	def __insert_aux(self,new_node,current_node):

		if new_node.getKey() < current_node.getKey():
			if current_node.hasLeftSon():
				current_node = current_node.getLeftSon()
				self.__insert_aux(new_node,current_node)
			else:
				current_node.setLeftSon(new_node)

		else:
			if current_node.hasRightSon():
				current_node = current_node.getRightSon()
				self.__insert_aux(new_node,current_node)
			else:
				current_node.setRightSon(new_node)
			


	def __str__(self):
		return str(self.root)