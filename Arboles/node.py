class Node:

	def __init__(self, key, value, father = None, left_son = None, right_son = None):
		self.__father = father
		self.__leftSon = left_son
		self.__rightSon = right_son
		self.__key = key
		self.__value = value


	def getLeftSon(self):
		return self.__leftSon



	def setLeftSon(self,new_node):
		self.__leftSon = new_node



	def getRightSon(self):
		return self.__rightSon



	def setRightSon(self,new_node):
		self.__rightSon = new_node



	def getKey(self):
		return self.__key



	def getValue(self):
		return self.__value



	def setValue(self,new_value):
		self.__value = new_value



	def getFather(self):
		return self.__father



	def hasLeftSon(self):
		return self.__leftSon



	def hasRightSon(self):
		return self.__rightSon



	def isLeftSon(self):
		return self.__father and self.__father.__leftSon == self



	def isRightSon(self):
		return self.__father and self.__father.__rightSon == self



	def isRoot(self):
		return not self.__father



	def isLeaf(self):
		return not (self.__rightSon or self.__leftSon)



	def haveBothChildren(self):
		return self.__rightSon and self.__leftSon



	def setNodeData(self,key,value,left_son,right_son):
		pass


	def __str__(self):
		chain = (str(self.__key)+
				", "+self.__value +
				", "+ str(self.__father) +
				", "+ str(self.__leftSon) +
				", "+ str(self.__rightSon)+"\n")
		return chain