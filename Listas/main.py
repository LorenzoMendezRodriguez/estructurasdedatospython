from lista_circular_doble import *
from lista_circular_simple import * 


''' 
*************************************************************************************
************************ Tecnológico de Costa Rica **********************************
*************************************************************************************
*************************************************************************************
************************ Lorenzo Méndez Rodríguez ***********************************
*************************************************************************************
*-----------------------------------------------------------------------------------*
************************* Estructuras de datos **************************************
************************* Listas enlazadas ******************************************
************************* Lista circular simple *************************************
************************* Lista circular doble **************************************
************************* Pila ******************************************************
************************* Grafo *****************************************************
************************* Arboles ***************************************************
-------------------------------------------------------------------------------------
'''

#Acrónimos
#SCL = simple circular list
#DCL = double circular list

class Main:

    def __init__(self):

        SCL = simpleCircularList()
        DCL = doubleCircularList()


        SCL.setLastNode("Bananas")
        SCL.setLastNode("Peras")
        SCL.setLastNode("Uvas")
        SCL.setLastNode("Manzanas")
        SCL.setLastNode("Fresas")
       
        #SCL.deleteFirstNode()
        #SCL.deleteLastNode()
        #SCL.deleteNodeByPosition(-1)
        #SCL.deleteNodeByName("Manzanas")

        DCL.setLastNode("Uva")
        DCL.setLastNode("Manzana")
        DCL.setLastNode("Banana")
        DCL.setLastNode("Naranja")
        DCL.setLastNode("Sandia")

        #DCL.deleteFirstNode()
        #DCL.deleteLastNode()
        #DCL.deleteNodeByName("Naranja")
        #DCL.deleteNodeByPosition(4)


        print("************************ Simple circular list *********************************\n")
        SCL.showSimpleCircularList()


        print("\n************************ Double circular list *********************************\n")
        DCL.showListFirstToLast()
        print()
        DCL.showListLastToFirst()

Main()
