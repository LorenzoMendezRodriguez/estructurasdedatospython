from nodoDCL import * #DCL = double circular list
from lista import *

class doubleCircularList(List):


	def setFirstNode(self,node_name):
		newNode = NodeDCL(node_name)

		if self.getEmptyList():
			self.first = newNode
			self.last = newNode

			self.last.next = self.first
			self.first.ant = self.last

		else:
			self.last.next = newNode
			newNode.ant = self.last

			self.first.ant = newNode
			newNode.next = self.first
			

			self.first = newNode

			

			
	def setLastNode(self,node_name):
		newNode = NodeDCL(node_name)

		if self.getEmptyList():
			self.first = newNode
			self.last = newNode

			self.last.next = self.first
			self.first.ant = self.last

		else:
			self.last.next = newNode 
			self.first.ant = newNode

			newNode.next = self.first
			newNode.ant = self.last

			self.last = newNode




	def deleteFirstNode(self):
		if self.getEmptyList():
			print("The double circular list is empty")

		elif self.first == self.last:
			self.first = None
			self.last = None
			print("element deleted")

		else:
			self.first = self.first.next
			print("element deleted")





	def deleteLastNode(self):
		if self.getEmptyList():
			print("The double circular list is empty")

		elif self.first == self.last:
			self.first = None
			self.last = None
			print("element deleted")

		else:
			self.last = self.last.ant
			print("element deleted")





	def deleteNodeByName(self,node_name):
		actual =  self.first
		prev = None

		if self.getEmptyList():
			print("The simple circular list is empty")


		if not self.theNodeExist(node_name):
			print("The element not exist")

		while actual.getElement() != node_name:
			prev = actual
			actual = actual.next


		if prev == None:
			self.first = actual.next

		elif actual == self.last:
			self.last = prev

		else:
			prev.next = actual.next
			(actual.next).ant = prev




	def deleteNodeByPosition(self,node_pos):
		actual = self.first
		prev = None
		ind = 0

		if self.getEmptyList():
			print("The simple circular list is empty")

		if not ( 0 <= node_pos < self.listSize()): 
			print("This position not exist in the list")

		while ind != node_pos:
			prev = actual
			actual = actual.next
			ind+=1

		if prev == None:
			self.first = actual.next

		elif actual == self.last:
			self.last = prev

		else:
			prev.next = actual.next
			(actual.next).ant = prev




	def getNodePosition(self,node_name):

		if self.getEmptyList():
			print("The simple circular list is empty")

		if not self.theNodeExist(node_name):
			print("The element not exist")

		else:
			actual = self.first
			position = 0 

			while actual.getElement() != node_name:
				actual = actual.next
				position+=1

			return position





	def getNodeName(self,node_pos):
		if self.getEmptyList():
			print("The simple circular list is empty")

		if not ( 0 <= node_pos < self.listSize()): 
			print("This position not exist in the list")

		else:
			actual = self.first
			position = 0 

			while position != node_pos:
				position+=1
				actual = actual.next 

			return actual

	


	def showListFirstToLast(self):
		temp = self.first
		print(temp)
		while not temp == self.last:
			temp = temp.next
			print(temp)

			

	def showListLastToFirst(self):
		temp = self.last
		print(temp)
		while not temp == self.first:
			temp = temp.ant
			print(temp)