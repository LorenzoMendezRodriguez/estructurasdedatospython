class List(object):

	def __init__(self):
		self.first = None
		self.last = None



	def getEmptyList(self):
		return self.first == None



	def listSize(self):
		temp = self.first
		count = 1 

		while temp != self.last:
			temp = temp.next
			count+=1

		return count



	def theNodeExist(self,node_name):
		if self.getEmptyList():
			print("The list is empty")

		else:
			actual =  self.first

			while actual.getElement() != node_name:
				
				if actual == self.last:
					return False
				
				else:
					actual = actual.next

			return True