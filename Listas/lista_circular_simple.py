from nodoSCL import * # SCL = simple circular list
from lista import *

''' 
*************************************************************************************
************************ Tecnológico de Costa Rica **********************************
*************************************************************************************
*************************************************************************************
************************ Lorenzo Méndez Rodríguez ***********************************
*************************************************************************************
*-----------------------------------------------------------------------------------*
*************************Lista circular simple***************************************
-------------------------------------------------------------------------------------
'''

class simpleCircularList(List):


	def setFirstNode(self,node_name):
		newNode = NodeSCL(node_name)

		if self.getEmptyList():
			self.first = newNode
			self.last = newNode
			self.last.next = self.first

		else:
			self.last.next = newNode
			newNode.next = self.first
			self.first = newNode




	def setLastNode(self,node_name):
		newNode = NodeSCL(node_name)

		if self.getEmptyList():
			self.first = newNode
			self.last = newNode
			self.last.next = self.first

		else:
			self.last.next = newNode
			newNode.next = self.first
			self.last = newNode




	def deleteFirstNode(self):
		if self.getEmptyList():
			print("The simple circular list is empty")

		#if first node is also last node
		elif self.first == self.last:
			self.first = None
			self.last = None
			print("element deleted")


		else: 
			self.first = self.first.next
			print("element deleted")



	def deleteLastNode(self):
		if self.getEmptyList():
			print("The simple circular list is empty")

		#if last node is also first node
		elif self.first == self.last:
			self.first = None
			self.last = None
			print("element deleted")


		else: 
			temp = self.first

			while not temp.next == self.last:
				temp = temp.next
					
			self.last = temp
			print("element deleted")




	def deleteNodeByName(self,node_name):

		if self.getEmptyList():
			print("The simple circular list is empty")


		if not self.theNodeExist(node_name):
			print("The element not exist")


		else: 
			actual = self.first
			prev = None

			while actual.getElement() != node_name:
				prev = actual
				actual =  actual.next

			if prev == None:
				self.first = actual.next

			elif actual == self.last:
				self.last = prev

			else:
				prev.next = actual.next




	def deleteNodeByPosition(self,node_pos):
		ind = 0 
		actual = self.first
		prev = None

		if self.getEmptyList():
			print("The simple circular list is empty")


		if not ( 0 <= node_pos < self.listSize()): 
			print("This position not exist in the list")

		else:
			# find the previous
			while ind < node_pos:
				prev = actual
				actual = actual.next
				ind+=1

			# if the node is the first
			if prev == None:
				self.first = actual.next
				print("delete element")

			#if the node is the last
			elif actual == self.last:
				self.last = prev
				print("delete element")

			#if the node is not the first or the last 
			else:
				prev.next = actual.next
				print("delete element")



	def getNodePosition(self,node_name):
		if self.getEmptyList():
			print("The list is empty")

		if not self.theNodeExist(node_name):
			print("The element not exist")

		else:
			actual = self.first
			ind = 0

			while actual.getElement() != node_name:
				actual = actual.next
				ind+=1

			return ind



	def getNodeName(self,node_pos):
		if self.getEmptyList():
			print("The list is empty")


		if not ( 0 <= node_pos < self.listSize()): 
			print("This position not exist in the list")

		else:
			actual = self.first
			ind = 0 

			while ind != node_pos:
				ind+=1
				actual = actual.next

			return actual




	def showSimpleCircularList(self):

		if self.getEmptyList():
			print("The simple circular list is empty")

		else:
			temp = self.first
			print(temp)

			while not  temp == self.last:
				temp = temp.next
				print(temp)
