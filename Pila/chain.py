from stack import *

''' 
*************************************************************************************
************************ Tecnológico de Costa Rica **********************************
*************************************************************************************
*************************************************************************************
************************ Lorenzo Méndez Rodríguez ***********************************
*************************************************************************************
*-----------------------------------------------------------------------------------*
**************** Usando la Pila para invertir una cadena de caracteres **************
-------------------------------------------------------------------------------------
'''

class Chain:

    #Constructor
    def __init__(self):
        self.stack = Stack()#instance


    #Remove the chain spaces
    def deleteSpaces(self,string):
        withoutSpaces = ""

        for char in string:
            if char != ' ':
                withoutSpaces +=char

        return withoutSpaces

    #Invert a string without using the stack
    def reverseStringWithoutStack(self,string):
        newString = ""
        stringSize =  len(string)-1

        while stringSize >=0:
            if string[stringSize] != ' ':
                newString+= string[stringSize]
            stringSize-=1

        return newString
            

    #Invert a string using the stack
    def reverseStringWithStack(self,string):
        newString = ""
        
        for char in string:
            if char != ' ':
                self.stack.pushItem(char)

        while not self.stack.itIsEmpty():
            newString+=self.stack.popItem()
            
        return newString

    #Is palindrome using the stack?
    def isPalindromeWithStack(self,string):
        return self.deleteSpaces(string) == self.reverseStringWithStack(string)

    #Is palindrome without using the stack?
    def isPalindromeWithoutStack(self,string):
        return self.deleteSpaces(string) == self.reverseStringWithoutStack(string)
